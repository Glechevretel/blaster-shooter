
#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Annoucement.generated.h"

/**
 * 
 */
UCLASS()
class BLASTER_API UAnnoucement : public UUserWidget
{
	GENERATED_BODY()

public:

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* WarmupTime;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* AnnoucementText;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* InfoText;
	
};
