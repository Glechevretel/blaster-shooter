#include "ProjectileRocket.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraFunctionLibrary.h"
#include "Projectile.h"
#include "Components/BoxComponent.h"
#include "Sound/SoundCue.h"
#include "NiagaraComponent.h"
#include "NiagaraSystemInstance.h"
#include "Sound/SoundCue.h"
#include "Components/AudioComponent.h"
#include "RocketMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Blaster/Character/BlasterCharacter.h"


AProjectileRocket::AProjectileRocket()
{
	RocketMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Rocket Mesh"));
	RocketMesh->SetupAttachment(GetRootComponent());
	RocketMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	RocketMouvementComponent = CreateDefaultSubobject<URocketMovementComponent>(TEXT("Rocket Movement Component"));
	RocketMouvementComponent->bRotationFollowsVelocity = true;
	RocketMouvementComponent->SetIsReplicated(true);

	ExplosionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Explosion Sphere"));
	ExplosionSphere->SetupAttachment(RootComponent);
	ExplosionSphere->SetSphereRadius(0.f, false);
}

void AProjectileRocket::BeginPlay()
{
	Super::BeginPlay();

	if (!HasAuthority())
	{
		CollisionBox->OnComponentHit.AddDynamic(this, &ThisClass::OnHit);
	}

	if (TrailSystem)
	{
		TrailSystemComponent = UNiagaraFunctionLibrary::SpawnSystemAttached(TrailSystem,
			GetRootComponent(),FName("RocketSpawnPoint"), GetActorLocation(), GetActorRotation(), EAttachLocation::KeepWorldPosition, false);
	}

	if (ProjectileLoop && LoopingSoundAttunation)
	{
		ProjectileLoopComponent = UGameplayStatics::SpawnSoundAttached(ProjectileLoop, GetRootComponent(), FName(), GetActorLocation(),
			EAttachLocation::KeepWorldPosition, false, 1.f, 1.f, 0.f, LoopingSoundAttunation, (USoundConcurrency*)nullptr, false);
	}

	ExplosionSphere->OnComponentBeginOverlap.AddDynamic(this, &ThisClass::OnSphereOverlap);
}

void AProjectileRocket::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor == GetOwner())
	{
		//UE_LOG(LogTemp, Warning, TEXT("Hit Self"));
		return;
	}

	APawn* FiringPawn = GetInstigator();
	if (FiringPawn && HasAuthority())
	{
		AController* FiringController = FiringPawn->GetController();
		if (FiringController)
		{
			AActor* CurrentActor = Cast<AActor>(FiringPawn);
			TArray<AActor*> ActorsToIgnore;
			ActorsToIgnore.Add(CurrentActor);
			UGameplayStatics::ApplyRadialDamageWithFalloff(this,Damage,10.f,GetActorLocation(), InnerDamageRadius, OuterDamageRadius,1.f,UDamageType::StaticClass(),
				TArray<AActor*>(), this, FiringController);
		}
	}

	ExplosionSphere->SetSphereRadius(350.f, true);

	// Setting a timer
	GetWorldTimerManager().SetTimer(DestroyTimer, this, &AProjectileRocket::DestroyTimerFinished, DestroyTime);

	// Spawning particles and sound
	if (ImpactParticles)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactParticles, GetActorTransform());
	}
	if (ImpactSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, ImpactSound, GetActorLocation());
	}
	// Disabling mesh visibility and collision
	if (RocketMesh) 
	{
		RocketMesh->SetVisibility(false);
	}
	if (CollisionBox)
	{
		CollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	// Stop spawning particles
	if (TrailSystemComponent && TrailSystemComponent->GetSystemInstance())
	{
		TrailSystemComponent->GetSystemInstance()->Deactivate();
	}

	// Stop looping sound
	if (ProjectileLoopComponent && ProjectileLoopComponent->IsPlaying())
	{
		ProjectileLoopComponent->Stop();
	}
}

void AProjectileRocket::DestroyTimerFinished()
{
	Destroy();
}

void AProjectileRocket::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ABlasterCharacter* BlasterCharacter = Cast<ABlasterCharacter>(OtherActor);
	if (BlasterCharacter)
	{
		auto FromLocation = GetActorLocation();
		auto PlayerLocation = BlasterCharacter->GetActorLocation();

		auto AwayVector = PlayerLocation - FromLocation;
		AwayVector = AwayVector.GetSafeNormal();
		AwayVector = AwayVector * ImpulseStrength;

		BlasterCharacter->LaunchCharacter(AwayVector, false, true);
	}
}

void AProjectileRocket::Destroyed()
{

}
